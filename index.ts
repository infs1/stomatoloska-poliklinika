import { API } from "./api";
import { Service } from "./service";
import { PostgresDAO } from "./postgresDao";

const dao = new PostgresDAO();
console.log(dao.getUserByOIB('00000000000'));
console.log(dao.getDentistByOIB('00000000000'));
console.log(dao.getPlaces());
dao.init().then(()=>{
    const service = new Service(dao);
    const api = new API(service);
});
