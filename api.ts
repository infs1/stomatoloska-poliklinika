
import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';
import session from 'express-session';
import bodyParser from 'body-parser';
import path from 'path';
import cookieParser from 'cookie-parser';
import { IService } from './iservice';
import { ErrorInfo, Korisnik, Mjesto, Pacijent, Specijalizacija, Termin, Tretman, Zahvat } from './models';
declare module 'express-session' {
    interface SessionData {
      user: any,
      isDentist: boolean,
    }
  }
class API{
    private app: Express;
    private port:string|undefined;
    private service:IService;
    constructor(service:IService){
        dotenv.config();
        this.app = express();
        this.port = process.env.PORT;
        this.service = service;


       
        this.app.set('views', path.join(__dirname, 'views'));
        this.app.set('view engine', 'ejs');
        this.app.use(express.json({limit: 50*1024*1024}));
        this.app.use(express.urlencoded({ extended: false, limit: 50*1024*1024 }));
        this.app.use(cookieParser());
        this.app.use(express.static(path.join(__dirname, 'public')));
        this.app.use(bodyParser.urlencoded({ extended: true, limit: 50*1024*1024 }));
        this.app.use(session({ secret: 'ytLtvLVn4MdzZsIQXIJd3hHtr2feegLD', cookie: { maxAge: 2*24*60*60*1000 }}))


        this.app.get('/', (req: Request, res: Response) => {
            console.log(req.session.user);
            res.render('frontpageView', {
                linkActive:"home",
                user: req.session.user
            });
        });

        this.app.get('/history', (req: Request, res: Response) => {
            res.render('historyView', {
                linkActive:"history",
                user: req.session.user
            });
        });

        this.app.post('/register', async (req:Request, res:Response) => {
            console.log(req);
            var result:ErrorInfo = await service.createRegistration(
                req.body.oib,req.body.name,req.body.surname,req.body.mail,req.body.password,req.body.idcard,req.body.place
            )
            console.log(result);
            
            if(result.statusCode==0){
                res.status(400).json({error:result.description})
            }else
                res.status(200).send('OK'); //samo za testiranje
        })

        this.app.get('/register', async (req: Request, res: Response) => {
            res.render('registerView', {
                linkActive:"",
                user: req.session.user,
                mjesta: await service.fetchMjesta()
            });
        });

        this.app.get('/dentists', async (req: Request, res: Response) => {
            res.render('dentistsView', {
                linkActive:"dentists",
                activeTable: {title:"Dentists", code:"dentists"},
                dentists: await service.fetchStomatolozi()
            }); //treba slati stomatologe iz baze
        });

        this.app.post('/schedule', (req:Request, res:Response) => {
            console.log(req)
            console.log(req.body);
            console.log(req.session.user);
            
            var result = this.service.createTermin(new Date(req.body.termin.datetime), req.session.user.pacientId, req.body.termin.dentistId, req.body.termin.treatmentId);
            console.log("-----------------------------------------------------");
            console.log(result);
            res.sendStatus(200);

        })

        this.app.get('/schedule', async (req: Request, res: Response) => {
            var termini:Termin[] = [];
            if(req.session.user)
                termini = await service.fetchTermini(req.session.user, req.session.isDentist!);
            console.log(termini);
            res.render('scheduleView', {
                linkActive:"schedule",
                user: req.session.user,
                dentists: await service.fetchStomatolozi(),
                treatments: await service.fetchTretmani(),
                appointments: termini
            }); //slati rezervirane termine iz baze
        });


        this.app.get('/admin', async(req: Request, res: Response) => {
            res.render('adminView', {
                linkActive:"",
                activeTable: {title:"", code:""},
                user: req.session.user,
                error: null,
                registracije: await service.fetchUnfinishedRegistrations()
            }); //slati rezervirane termine iz baze
        });
        this.app.post('/admin/registracija/finish', async(req: Request, res: Response) => {
            console.log(req.body);
            await service.finishRegistration(req.body.id, req.body.accept);
            res.sendStatus(200);
        });
        //---------------admin-----------------------------------
        this.app.get('/admin/mjesto', async(req: Request, res: Response) => {
            var mjesta = await service.fetchMjesta()
            if(req.query.search){
                var search = JSON.parse(req.query.search as string);
                if(search)
                {   
                    mjesta = Mjesto.search(search, mjesta)
                }
            }
            res.render('adminView', {
                linkActive:"mjesto",
                activeTable: {title:"Mjesto", code:"mjesto"},
                user: req.session.user,
                mjesta: mjesta,
                error: null
            }); //slati rezervirane termine iz baze
        });
        this.app.get('/admin/mjesto/single/:id', async(req: Request, res: Response) => {
            try {
                var id = Number.parseInt(req.params.id)
                res.render('adminView', {
                    linkActive:"mjesto",
                    activeTable: {title:"Mjesto", code:"mjestoSingle"},
                    user: req.session.user,
                    mjesto: await service.fetchMjestoById(id),
                    error: null
                }); //slati rezervirane termine iz baze
            } catch (error) {
                console.log("error ", error);
                res.redirect('/admin/mjesto'); //slati rezervirane termine iz baze
            }
            
        });
        this.app.post('/admin/mjesto/action/:action', async(req: Request, res: Response) => {

            var mjesto = req.body.mjesto;
            console.log(mjesto)
            console.log(req.params.action)
            switch(req.params.action){
                case 'update':{await service.updateMjesto(mjesto)};break;
                case 'delete':{await service.deleteMjesto(mjesto)};break;
                case 'create':{await service.createMjesto(mjesto)};break;
                default : res.redirect('/admin/mjesto/');
            }

            res.redirect('/admin/mjesto/'); //slati rezervirane termine iz baze
        });

        //-------------------------------tretman------------------
        this.app.get('/admin/tretman', async(req: Request, res: Response) => {
            var tretmani =await service.fetchTretmani();
            if(req.query.search){
                var search = JSON.parse(req.query.search as string);
                if(search)
                {   
                    tretmani = Tretman.search(search, tretmani)
                }
            }
            res.render('adminView', {
                linkActive:"tretman",
                activeTable: {title:"Tretman", code:"tretman"},
                user: req.session.user,
                tretmani: tretmani,
                error: null
            }); //slati rezervirane termine iz baze
        });
        this.app.get('/admin/tretman/single/:id', async(req: Request, res: Response) => {
            try {
                var id = Number.parseInt(req.params.id)
                res.render('adminView', {
                    linkActive:"tretman",
                    activeTable: {title:"Tretman", code:"tretmanSingle"},
                    user: req.session.user,
                    tretman: await service.fetchTretmanById(id),
                    error: null
                }); //slati rezervirane termine iz baze
            } catch (error) {
                console.log("error ", error);
                res.redirect('/admin/tretman'); //slati rezervirane termine iz baze
            }
            
        });
        this.app.post('/admin/tretman/action/:action', async(req: Request, res: Response) => {

            var tretman = req.body.tretman;
            console.log(tretman)
            console.log(req.params.action)
            switch(req.params.action){
                case 'update':{await service.updateTretman(tretman.id, tretman.naziv, tretman.cijena)};break;
                case 'delete':{await service.deleteTretman(tretman.id)};break;
                case 'create':{await service.createTretman(tretman.naziv, tretman.cijena)};break;
                default : res.redirect('/admin/tretman/');
            }

            res.redirect('/admin/tretman/'); //slati rezervirane termine iz baze
        });
        //-------------------------------tretman------------------

        //-------------------------------termin------------------
        this.app.get('/admin/termin', async(req: Request, res: Response) => {
            console.log(await service.fetchAllTermini());
            
            res.render('adminView', {
                linkActive:"termin",
                activeTable: {title:"Termini", code:"termin"},
                user: req.session.user,
                termini: await service.fetchAllTermini(),
                pacijenti: await service.fetchPacijenti(),
                stomatolozi: await service.fetchStomatologBasic(),
                tretmani: await service.fetchTretmani(),
                error: null
            }); //slati rezervirane termine iz baze
        });
        this.app.get('/admin/termin/single/:id', async(req: Request, res: Response) => {
            try {
                var id = Number.parseInt(req.params.id)
                res.render('adminView', {
                    linkActive:"termin",
                    activeTable: {title:"Termini", code:"terminSingle"},
                    user: req.session.user,
                    termin: await service.fetchTerminById(id),
                    pacijenti: await service.fetchPacijenti(),
                    stomatolozi: await service.fetchStomatologBasic(),
                    tretmani: await service.fetchTretmani(),
                    error: null
                }); //slati rezervirane termine iz baze
            } catch (error) {
                console.log("error ", error);
                res.redirect('/admin/termin'); //slati rezervirane termine iz baze
            }
            
        });
        this.app.post('/admin/termin/action/:action', async(req: Request, res: Response) => {

            var termin = req.body.termin;
            console.log(termin)
            console.log(req.params.action)
            switch(req.params.action){
                case 'update':{await service.updateTermin(termin.id, termin.datum, termin.pacijent, termin.stomatolog, termin.tretman)};break;
                case 'delete':{await service.deleteTermin(termin.id)};break;
                case 'create':{await service.createTermin(termin.datum, termin.pacijent, termin.stomatolog, termin.tretman)};break;
                default : res.redirect('/admin/termin/');
            }

            res.redirect('/admin/termin/'); //slati rezervirane termine iz baze
        });
        //-------------------------------termin------------------
        //-------------------------------korisnik------------------
        this.app.get('/admin/korisnik', async(req: Request, res: Response) => {
            console.log(await service.fetchAllTermini());
            
            res.render('adminView', {
                linkActive:"termin",
                activeTable: {title:"Korisnici", code:"korisnik"},
                user: req.session.user,
                korisnici: await service.fetchKorisnici(),
                pacijenti: await service.fetchPacijenti(),
                stomatolozi: await service.fetchStomatologBasic(),
                mjesta: await service.fetchMjesta(),
                error: null
            }); //slati rezervirane termine iz baze
        });
        this.app.get('/admin/korisnik/single/:id', async(req: Request, res: Response) => {
            try {
                var id = req.params.id;
                res.render('adminView', {
                    linkActive:"korisnik",
                    activeTable: {title:"Korisnici", code:"korisnikSingle"},
                    user: req.session.user,
                    korisnik: await service.fetchKorisnikByOIB(id),
                    pacijenti: await service.fetchPacijenti(),
                    stomatolozi: await service.fetchStomatologBasic(),
                    mjesta: await service.fetchMjesta(),
                    error: null
                }); //slati rezervirane termine iz baze
            } catch (error) {
                console.log("error ", error);
                res.redirect('/admin/termin'); //slati rezervirane termine iz baze
            }
            
        });
        this.app.post('/admin/korisnik/action/:action', async(req: Request, res: Response) => {

            var korisnik = req.body.korisnik;
            console.log(korisnik)
            console.log(req.params.action)
            switch(req.params.action){
                case 'update':{await service.updateKorisnik(
                    korisnik.oib,
                    korisnik.ime,
                    korisnik.prezime,
                    korisnik.email,
                    korisnik.mjesto,
                    korisnik.password,
                    korisnik.stomatolog,
                    korisnik.pacijent,
                    korisnik.isAdmin
                )};break;
                case 'delete':{await service.deleteKorisnik(korisnik.oib)};break;
                case 'create':{};break;
                default : res.redirect('/admin/korisnik/');
            }

            res.redirect('/admin/korisnik/'); //slati rezervirane termine iz baze
        });
        //-------------------------------korisnik------------------
        //-------------------------PACIJENT----------------------------
        this.app.get('/admin/pacijent', async(req: Request, res: Response) => {
            console.log(await service.fetchPacijenti());
            
            res.render('adminView', {
                linkActive:"pacijent",
                activeTable: {title:"Pacijent", code:"pacijent"},
                user: req.session.user,
                pacijenti: await service.fetchPacijenti(),
                error: null
            }); //slati rezervirane termine iz baze
        });
        this.app.get('/admin/pacijent/single/:id', async(req: Request, res: Response) => {
            try {
                console.log(req);
                
                var id = Number.parseInt(req.params.id)
                res.render('adminView', {
                    linkActive:"pacijent",
                    activeTable: {title:"Pacijent", code:"pacijentSingle"},
                    user: req.session.user,
                    pacijent: await service.fetchPacijentById(id),
                    error: null
                }); //slati rezervirane termine iz baze
            } catch (error) {
                console.log("error ", error);
                res.redirect('/admin/pacijent'); //slati rezervirane termine iz baze
            }
        });
        this.app.post('/admin/pacijent/action/:action', async(req: Request, res: Response) => {

            var pacijent = req.body.pacijent;
            console.log(pacijent)
            console.log(req.params.action)
            switch(req.params.action){
                case 'update':{await service.updatePacijent(pacijent.id, pacijent.statusZuba)};break;
                case 'delete':{await service.deletePacijent(pacijent.id)};break;
                case 'create':{await service.createPacijent(pacijent.statusZuba)};break;
                default : res.redirect('/admin/pacijent/');
            }

            res.redirect('/admin/pacijent/'); 
        });
        //-------------------------PACIJENT----------------------------

        //-------------------------ZAHVAT----------------------------
        this.app.get('/admin/zahvat', async(req: Request, res: Response) => {
            var sorter = req.query.sort;
            var asc = (req.query.asc=='false')?false:true;
            var zahvati = Zahvat.sortBy((sorter as string || 'serijskiBroj'), await service.fetchZahvati(), asc);
            if(req.query.search){
                var search = JSON.parse(req.query.search as string);
                if(search)
                {   
                    zahvati = Zahvat.search(search, zahvati)
                }
            }
            res.render('adminView', {
                linkActive:"zahvat",
                activeTable: {title:"Zahvat", code:"zahvat"},
                user: req.session.user,
                zahvati: zahvati,
                pacijenti: await service.fetchPacijenti(),
                nextOrder:!asc,
                error: null,
            }); //slati rezervirane termine iz baze
        });
        this.app.get('/admin/zahvat/single/:id', async(req: Request, res: Response) => {
            try {
                console.log(req);
                
                var id = Number.parseInt(req.params.id)
                res.render('adminView', {
                    linkActive:"zahvat",
                    activeTable: {title:"Zahvat", code:"zahvatSingle"},
                    user: req.session.user,
                    zahvat: await service.fetchZahvatById(id),
                    error: null
                }); //slati rezervirane termine iz baze
            } catch (error) {
                console.log("error ", error);
                res.redirect('/admin/zahvat'); //slati rezervirane termine iz baze
            }
        });
        this.app.post('/admin/zahvat/action/:action', async(req: Request, res: Response) => {

            var zahvat = req.body.zahvat;
            console.log(zahvat)
            console.log(req.params.action)
            switch(req.params.action){
                case 'update':{await service.updateZahvat(zahvat.id, zahvat.date, zahvat.desc, zahvat.pacientId)};break;
                case 'delete':{await service.deleteZahvat(zahvat.id)};break;
                case 'create':{await service.createZahvat(zahvat.date, zahvat.desc, zahvat.pacientId)};break;
                default : res.redirect('/admin/zahvat/');
            }

            res.redirect('/admin/zahvat/'); 
        });

        
        //-------------------------ZAHVAT----------------------------


        //-----------------------STOMATOLOG---------------------
        this.app.get('/admin/stomatolog', async(req: Request, res: Response) => {
            res.render('adminView', {
                linkActive:"stomatolog",
                activeTable: {title:"Stomatolog", code:"stomatolog"},
                user: req.session.user,
                specijalizacije: await service.fetchSpecijalizacije(),
                stomatolozi: await service.fetchStomatologBasic(),
                error: null
            }); //slati rezervirane termine iz baze
        });

        this.app.get('/admin/stomatolog/single/:id', async(req: Request, res: Response) => {
            console.log(req);
            try {
                
                var id = Number.parseInt(req.params.id)
                console.log("id:",id);
                
                console.log(await service.fetchStomatologByIdBasic(id));
                res.render('adminView', {
                    linkActive:"stomatolog",
                    activeTable: {title:"Stomatolog", code:"stomatologSingle"},
                    user: req.session.user,
                    stomatolog: await service.fetchStomatologByIdBasic(id),
                    specijalizacije: await service.fetchSpecijalizacije(),
                    error: null
                }); //slati rezervirane termine iz baze
            } catch (error) {
                console.log("error ", error);
                res.redirect('/admin/stomatolog'); //slati rezervirane termine iz baze
            }
            
        });
        this.app.post('/admin/stomatolog/action/:action', async(req: Request, res: Response) => {

            var stomatolog = req.body.stomatolog;
            console.log(req)
            console.log(req.params.action)
            switch(req.params.action){
                case 'update':{await service.updateStomatolog(stomatolog.id, stomatolog.specijalizacija)};break;
                case 'delete':{await service.deleteStomatolog(stomatolog.id)};break;
                case 'create':{await service.createStomatolog(stomatolog.specijalizacija)};break;
                default : res.redirect('/admin/stomatolog/');
            }

            res.redirect('/admin/stomatolog/'); //slati rezervirane termine iz baze
        });
        
        //-----------------------STOMATOLOG---------------------
        
        //---------------admin-----------------------------------

        // ----------------SPECIJALIZACIJA-------------------------
        this.app.get('/admin/specijalizacija', async(req: Request, res: Response) => {
            var specijalizacije = await service.fetchSpecijalizacije()
            if(req.query.search){
                var search = JSON.parse(req.query.search as string);
                if(search)
                {   
                    specijalizacije = Specijalizacija.search(search, specijalizacije)
                }
            }
            res.render('adminView', {
                linkActive:"specijalizacija",
                activeTable: {title:"Specijalizacija", code:"specijalizacija"},
                user: req.session.user,
                specijalizacije: specijalizacije,
                error: null
            }); //slati rezervirane termine iz baze
        });

        this.app.get('/admin/specijalizacija/single/:id', async(req: Request, res: Response) => {
            try {
                console.log(req);
                
                var id = Number.parseInt(req.params.id)
                res.render('adminView', {
                    linkActive:"specijalizacija",
                    activeTable: {title:"Specijalizacija", code:"specijalizacijaSingle"},
                    user: req.session.user,
                    specijalizacija: await service.fetchSpecijalizacijaById(id),
                    error: null
                }); //slati rezervirane termine iz baze
            } catch (error) {
                console.log("error ", error);
                res.redirect('/admin/specijalizacija'); //slati rezervirane termine iz baze
            }
            
        });
        this.app.post('/admin/specijalizacija/action/:action', async(req: Request, res: Response) => {

            var specijalizacija = req.body.specijalizacija;
            console.log(req)
            console.log(req.params.action)
            switch(req.params.action){
                case 'update':{await service.updateSpecijalizacija(specijalizacija.id, specijalizacija.naziv)};break;
                case 'delete':{await service.deleteSpecijalizacija(specijalizacija.id)};break;
                case 'create':{await service.createSpecijalizacija(specijalizacija.naziv)};break;
                default : res.redirect('/admin/mjesto/');
            }

            res.redirect('/admin/mjesto/'); //slati rezervirane termine iz baze
        });
        // ----------------SPECIJALIZACIJA-------------------------

        this.app.listen(this.port, () => {
            console.log(`⚡️[server]: Server is running at http://localhost:${this.port}`);
        });
        
        this.app.post('/login', async (req:Request, res:Response) => {
            var result:Korisnik|ErrorInfo = await this.service.login(req.body.mail, req.body.password, req.body.isDentist);
            console.log("-----------------------------------------------------");
            console.log(result);
            if(result instanceof ErrorInfo){
                res.status(401).send('NOT OK');
            }else{
                req.session.user = result;
                if(req.session.user.isAdmin){
                    res.json({
                        "isAdmin": true
                    })
                } else {
                    res.json({
                        "isAdmin": false
                    })
                }
            }
        })

        this.app.get('/login', async (req:Request, res:Response) => {
            //var korisnik:Korisnik = await service.getUserByOIB('00000000000') as Korisnik;
            //req.session.user = korisnik;
            res.render('loginView', {
                linkActive:"",
                user: req.session.user
            });
        })

        this.app.get('/logout', async (req:Request, res:Response) => {
            req.session.destroy((err) => {
                if(err) {
                    console.log(err);
                }
                else {
                    res.status(200).send("OK");
                }
              })
        })

        //testget
        this.app.get('/korisnik', async (req:Request, res:Response) => {
            //recimo da je poslao nesto;
            res.send(await this.service.getUser("12345678900"));
        })
    }

}

export {API};
