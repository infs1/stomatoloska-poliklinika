CREATE TABLE IF NOT EXISTS Pacijent
(
    IdPacijenta SERIAl,
    StatusZuba TEXT,
    PRIMARY KEY (IdPacijenta)
);

CREATE TABLE IF NOT EXISTS Specijalizacija
(
    IdSpecijalizacije SERIAl,
    NazivSpecijalizacije VARCHAR(25) NOT NULL,
    PRIMARY KEY (IdSpecijalizacije)
);

CREATE TABLE IF NOT EXISTS Tretman
(
    IdTretmana SERIAl,
    NazivTretmana VARCHAR(40) NOT NULL,
    CijenaTretmana INT NOT NULL,
    PRIMARY KEY (IdTretmana)
);

CREATE TABLE IF NOT EXISTS Mjesto
(
    IdMjesta SERIAL,
    PbrMjesta INT NOT NULL,
    NazivMjesta VARCHAR(20) NOT NULL,
    PRIMARY KEY (IdMjesta)
);

CREATE TABLE IF NOT EXISTS Zahvat
(
    SerijskiBrojZahvata SERIAL,
    DatumZahvata DATE NOT NULL,
    OpisZahvata VARCHAR(150) NOT NULL,
    IdPacijenta INT NOT NULL,
    PRIMARY KEY (SerijskiBrojZahvata),
    FOREIGN KEY (IdPacijenta) REFERENCES Pacijent(IdPacijenta)
);
CREATE TABLE IF NOT EXISTS Stomatolog
(
    IdStomatologa SERIAL,
    IdSpecijalizacije INT NOT NULL,
    PRIMARY KEY (IdStomatologa),
    FOREIGN KEY (IdSpecijalizacije) REFERENCES Specijalizacija(IdSpecijalizacije)
);
CREATE TABLE IF NOT EXISTS Korisnik
(
    OIB VARCHAR(11) NOT NULL,
    Ime VARCHAR(30) NOT NULL,
    Prezime VARCHAR(30) NOT NULL,
    Email VARCHAR(128) NOT NULL UNIQUE,
    Password VARCHAR(128) NOT NULL,
    IdMjesta INT NOT NULL,
    IdPacijenta INT,
    IdStomatologa INT,
    Administrator BOOLEAN DEFAULT FALSE,
    PRIMARY KEY (OIB),
    FOREIGN KEY (IdMjesta) REFERENCES Mjesto(IdMjesta),
    FOREIGN KEY (IdPacijenta) REFERENCES Pacijent(IdPacijenta),
    FOREIGN KEY (IdStomatologa) REFERENCES Stomatolog(IdStomatologa)
);
CREATE TABLE IF NOT EXISTS Termin
(
    SerijskiBrojTermina SERIAL,
    DatumIVrijemeTermina DATE NOT NULL,
    IdPacijenta INT NOT NULL,
    IdStomatologa INT NOT NULL,
    IdTretmana INT NOT NULL,
    PRIMARY KEY (SerijskiBrojTermina),
    FOREIGN KEY (IdPacijenta) REFERENCES Pacijent(IdPacijenta),
    FOREIGN KEY (IdStomatologa) REFERENCES Stomatolog(IdStomatologa),
    FOREIGN KEY (IdTretmana) REFERENCES Tretman(IdTretmana)
);
CREATE TABLE IF NOT EXISTS KorisnikRegistracija
(
    idRegistracije SERIAL,
    OIB VARCHAR(11) NOT NULL,
    Ime VARCHAR(30) NOT NULL,
    Prezime VARCHAR(30) NOT NULL,
    Email VARCHAR(128) NOT NULL UNIQUE,
    Password VARCHAR(128) NOT NULL,
    SlikaOsobne BYTEA NOT NULL,
    IdMjesta INT NOT NULL,
    isDentist BOOLEAN DEFAULT FALSE,
    finished BOOLEAN DEFAULT FALSE,
    accepted BOOLEAN DEFAULT FALSE,
    Administrator BOOLEAN DEFAULT FALSE,
    PRIMARY KEY (idRegistracije),
    FOREIGN KEY (IdMjesta) REFERENCES Mjesto(IdMjesta)
);
