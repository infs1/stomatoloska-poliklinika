import {Client} from 'pg';
import { ErrorInfo, Korisnik, Mjesto, Pacijent, Registracija, Specijalizacija, Stomatolog, Termin, Tretman, Zahvat } from './models';
import { DAO } from './dao';
import dotenv from 'dotenv';
// const client = new Client({
//     user: 'dbuser',
//     host: 'database.server.com',
//     database: 'mydb',
//     password: 'secretpassword',
//     port: 3211,
// })
// client.connect()
// client.query('SELECT NOW()', (err, res) => {
//     console.log(err, res)
//     client.end()
// })


/** DOTENV FILE
 * 
// @ts-ignore
PORT = 8080
// @ts-ignore
DBUSER = 'postgres'
// @ts-ignore
DBHOST = 'localhost'
// @ts-ignore
DBNAME = 'infsus'
// @ts-ignore
DBPASS = '123'
// @ts-ignore
DBPORT = 5432
 * 
 * 
 * 
 */
class PostgresDAO implements DAO{
    client:Client;
    constructor(){
        dotenv.config();
        this.client =  new Client({
                user: process.env.DBUSER,
                host: process.env.DBHOST,
                database: process.env.DBNAME,
                password: process.env.DBPASS,
                port: Number(process.env.DBPORT)
        })
    }
    async getAppointments() {
        var results = await this.client.query(`
        SELECT * FROM Termin`);
        console.log(results);
        var termini:Termin[] = [];
        if(results.rowCount > 0){
            for(var r of results.rows){
                termini.push(new Termin(  
                    r.serijskibrojtermina,
                    r.datumivrijemetermina,
                    await this.getPacientByIdBasic(r.idpacijenta) as Pacijent,
                    await this.getDentistByIdBasic(r.idstomatologa) as Stomatolog,
                    await this.getTreatmentById(r.idtretmana) as Tretman
                ));
            }
        }
        return termini;
    }
    createDentistBasic(idSpecijalizacije: any) {
        this.client.query(`INSERT INTO stomatolog
        (IdSpecijalizacije)
        VALUES ($1)`, 
        [idSpecijalizacije]).then((res)=>{}, err=>{
            throw err;
        });
    }
    async getSpecializations() {
        var results = await this.client.query(`
        SELECT * FROM Specijalizacija`);
        console.log(results);
        var specijalizacije:Specijalizacija[] = [];
        if(results.rowCount > 0){
            for(var r of results.rows)
            specijalizacije.push( new Specijalizacija(
                r.idspecijalizacije,
                r.nazivspecijalizacije
            ));
            
        }
        return specijalizacije;
    }
    async getProcedures() {
        var results = await this.client.query(`
        SELECT * FROM Zahvat`);
        console.log(results);
        var mjesta:Zahvat[] = [];
        if(results.rowCount > 0){
            for(var r of results.rows)
            mjesta.push( new Zahvat(  
                r.serijskibrojzahvata,
                new Date(r.datumzahvata), 
                r.opiszahvata,
                r.idpacijenta
            )
            );
            
        }
        return mjesta;
    }
    async getPacientByIdBasic(id: number) {
        var results = await this.client.query(`
        SELECT * FROM pacijent WHERE idpacijenta = $1`, [id]);
        console.log(results);
        if(results.rowCount > 0)
            return new Pacijent(
                results.rows[0].idpacijenta,
                results.rows[0].oib,
                results.rows[0].ime,
                results.rows[0].prezime,
                results.rows[0].email,
                results.rows[0].password,
                new Mjesto(results.rows[0].idmjesta,
                            results.rows[0].pbrmjesta, 
                           results.rows[0].nazivmjesta
                ),
                results.rows[0].administrator,
                await this.getProceduresFromPacient(results.rows[0].idpacijenta) as Zahvat[],
                results.rows[0].statuszuba
            )
        else return new ErrorInfo(0,"no such Pacient");
    }
    createBasicPacient(statusZuba: string) {
        this.client.query(`INSERT INTO pacijent
        (StatusZuba)
        VALUES ($1)`, 
        [statusZuba]).then((res)=>{}, err=>{
            throw err;
        });
    }
    
    deleteUserByOIB(oib: string) {
        this.client.query(`DELETE FROM Korisnik WHERE oib = $1`, 
        [oib]).then((res)=>{}, err=>{
            throw err;
        });
    }
    updateUserByOIB(oib: string, ime: string, prezime: string, email: string, password: string, idMjesta: number, idStomatologa: number|null, idPacijenta: number|null, administrator: boolean) {
        this.client.query(`UPDATE Korisnik SET 
        ime = $2,
        Prezime = $3,
        Email = $4,
        Password = $5,
        IdMjesta = $6,
        IdPacijenta = $7,
        IdStomatologa = $8,
        Administrator = $9
        WHERE oib = $1`, 
        [oib, ime, prezime, email, password, idMjesta, idPacijenta, idStomatologa, administrator]).then((res)=>{}, err=>{
            throw err;
        });
    }
    deletePacientByOIB(oib: string) {
        throw new Error('Method not implemented.');
    }
    deletePacientById(id: number) {
        this.client.query(`DELETE FROM Pacijent WHERE idPacijenta = $1`, 
        [id]).then((res)=>{}, err=>{
            throw err;
        });
    }
    updatePacientById(id: number, statusZuba: string) {
        this.client.query(`UPDATE Pacijent SET 
        StatusZuba = $2
        WHERE idPacijenta = $1`, 
        [id, statusZuba]).then((res)=>{}, err=>{
            throw err;
        });
    }
    deleteDentistByOIB(oib: string) {
        throw new Error('Method not implemented.');
    }
    deleteDentistById(id: number) {
        this.client.query(`DELETE FROM Stomatolog WHERE idStomatologa = $1`, 
        [id]).then((res)=>{}, err=>{
            throw err;
        });
    }
    updateDentistById(id: number, idSpecijalizacije: number) {
        this.client.query(`UPDATE Stomatolog SET 
        IdSpecijalizacije = $2
        WHERE IdStomatologa = $1`, 
        [id, idSpecijalizacije]).then((res)=>{}, err=>{
            throw err;
        });
    }
    deletePlaceById(id: number) {
        this.client.query(`DELETE FROM Mjesto WHERE idMjesta = $1`, 
        [id]).then((res)=>{}, err=>{
            throw err;
        });
    }
    deletePlaceByPbr(pbrMjesta: number) {
        this.client.query(`DELETE FROM Mjesto WHERE pbrMjesta = $1`, 
        [pbrMjesta]).then((res)=>{}, err=>{
            throw err;
        });
    }
    updatePlaceById(id: number, pbrMjesta: number, naziv: string) {
        this.client.query(`UPDATE Mjesto SET 
        PbrMjesta = $2,
        NazivMjesta = $3
        WHERE IdMjesta = $1`, 
        [id, pbrMjesta, naziv]).then((res)=>{}, err=>{
            throw err;
        });
    }
    updatePlaceByPbr(pbrMjesta: number, naziv: string) {
        this.client.query(`UPDATE Mjesto SET 
        NazivMjesta = $2
        WHERE PbrMjesta = $1`, 
        [pbrMjesta, naziv]).then((res)=>{}, err=>{
            throw err;
        });
    }
    deleteProcedureById(id: number) {
        this.client.query(`DELETE FROM Zahvat WHERE SerijskiBrojZahvata = $1`, 
        [id]).then((res)=>{}, err=>{
            throw err;
        });
    }
    deleteProcedureByPacientId(pacientId: number) {
        this.client.query(`DELETE FROM Zahvat WHERE idPacijenta = $1`, 
        [pacientId]).then((res)=>{}, err=>{
            throw err;
        });
    }
    updateProcedureById(id: number, date: Date, desc: string, pacientId: number) {
        this.client.query(`UPDATE Zahvat SET 
        DatumZahvata = $2,
        OpisZahvata = $3,
        IdPacijenta = $4
        WHERE SerijskiBrojZahvata = $1`, 
        [id, date, desc, pacientId]).then((res)=>{}, err=>{
            throw err;
        });
    }
    deleteAppointmentById(id: number) {
        this.client.query(`DELETE FROM Termin WHERE SerijskiBrojTermina = $1`, 
        [id]).then((res)=>{}, err=>{
            throw err;
        });
    }
    deleteAppointmentsForPacientById(pacientId: number) {
        this.client.query(`DELETE FROM Termin WHERE idPacijenta = $1`, 
        [pacientId]).then((res)=>{}, err=>{
            throw err;
        });
    }
    deleteAppointmentsForDentistById(dentistId: number) {
        this.client.query(`DELETE FROM Termin WHERE idStomatologa = $1`, 
        [dentistId]).then((res)=>{}, err=>{
            throw err;
        });
    }
    deleteAppointmentsForTreatmentById(treatmentId: number) {
        this.client.query(`DELETE FROM Termin WHERE idTretmana = $1`, 
        [treatmentId]).then((res)=>{}, err=>{
            throw err;
        });
    }
    deleteAppointmentsByDate(date: Date) {
        this.client.query(`DELETE FROM Termin WHERE DatumIVrijemeTermina = $1`, 
        [date]).then((res)=>{}, err=>{
            throw err;
        });
    }
    updateAppointment(id: number, date: Date, pacientId: number, dentistId: number, treatmentId: number) {
        this.client.query(`UPDATE Termin SET 
        DatumIVrijemeTermina = $2,
        IdPacijenta = $3,
        IdStomatologa = $4,
        IdTretmana = $5
        WHERE SerijskiBrojTermina = $1`, 
        [id, date, pacientId, dentistId, treatmentId]).then((res)=>{}, err=>{
            throw err;
        });
    }
    deleteSpecializationById(id: number) {
        this.client.query(`DELETE FROM Specijalizacija WHERE IdSpecijalizacije = $1`, 
        [id]).then((res)=>{}, err=>{
            throw err;
        });
    }
    updateSpecialization(id: number, name: string) {
        this.client.query(`UPDATE Specijalizacija SET 
        NazivSpecijalizacije = $2
        WHERE IdSpecijalizacije = $1`, 
        [id, name]).then((res)=>{}, err=>{
            throw err;
        });
    }
    deleteTreatmentById(id: number) {
        this.client.query(`DELETE FROM Tretman WHERE idTretmana = $1`, 
        [id]).then((res)=>{}, err=>{
            throw err;
        });
    }
    updateTreatment(id: number, name: string, price: number) {
        this.client.query(`UPDATE Tretman SET 
        NazivTretmana = $2,
        CijenaTretmana = $3
        WHERE IdTretmana = $1`, 
        [id, name, price]).then((res)=>{}, err=>{
            throw err;
        });
    }
   

    async finishRegistration(id: number, accepted:boolean, specializationId:number = -1) {
        var results = await this.client.query(`UPDATE KorisnikRegistracija SET finished = true, accepted = $2 WHERE idregistracije = $1 RETURNING *`, [id, accepted]);
        if(accepted){
            console.log(results);
            if(results.rows[0].isDentist){
                this.createDentist(
                    results.rows[0].oib,
                    results.rows[0].ime,
                    results.rows[0].prezime,
                    results.rows[0].email,
                    results.rows[0].password,
                    results.rows[0].idmjesta,
                    specializationId
                );
            }else{
                this.createPacient(
                    results.rows[0].oib,
                    results.rows[0].ime,
                    results.rows[0].prezime,
                    results.rows[0].email,
                    results.rows[0].password,
                    results.rows[0].idmjesta
                );
            }
        }
    }
    createUserRegistration(oib: string, ime: string, prezime: string, email: string, password:string, idMjesta:number, slikaOsobne:Blob, isDentist: boolean) {
        this.client.query(`INSERT INTO KorisnikRegistracija
        (oib, ime, prezime, email, password, slikaosobne, idmjesta, isdentist)
        VALUES ($1,$2,$3,$4,$5,$6,$7,$8)`, 
        [oib, ime, prezime, email, password, slikaOsobne, idMjesta, isDentist]).then((res)=>{}, err=>{
            throw err;
        });
    }
    async getUserRegistrations() {
        var results = await this.client.query(
            `SELECT * FROM KorisnikRegistracija as k join Mjesto as m on k.IdMjesta = m.IdMjesta`);
        console.log(results);
        if(results.rowCount > 0){
            var registracije:Registracija[] = [];
            for(var r of results.rows){
                registracije.push(
                    new Registracija(
                        r.idregistracije,
                        r.oib,
                        r.ime,
                        r.prezime,
                        r.email,
                        new Mjesto(r.idmjesta,
                                   r.pbrmjesta, 
                                   r.nazivmjesta
                        ),
                        r.administrator,
                        r.password,
                        r.slikaosobne,
                        r.isdentist,
                        r.finished,
                        r.accepted
                    )
                );
            }
            return registracije;
        }
             
        else return new ErrorInfo(0,"no such registration");
    }
    async getUserRegistrationById(id: number) {
        var results = await this.client.query(
            `SELECT * FROM KorisnikRegistracija as k join Mjesto as m on k.IdMjesta = m.IdMjesta where idRegistracije = $1`,[id]);
        console.log(results);
        if(results.rowCount > 0)
            return new Registracija(
                results.rows[0].idregistracije,
                results.rows[0].oib,
                results.rows[0].ime,
                results.rows[0].prezime,
                results.rows[0].email,
                new Mjesto(results.rows[0].idmjesta,
                            results.rows[0].pbrmjesta, 
                           results.rows[0].nazivmjesta
                ),
                results.rows[0].administrator,
                results.rows[0].password,
                results.rows[0].slikaosobne,
                results.rows[0].isdentist,
                results.rows[0].finished,
                results.rows[0].accepted
            )
        else return new ErrorInfo(0,"no such registration");
    }
    async getUserRegistrationByOIB(oib: string) {
        var results = await this.client.query(
            `SELECT * FROM KorisnikRegistracija as k join Mjesto as m on k.IdMjesta = m.IdMjesta where oib = $1`,[oib]);
        console.log(results);
        if(results.rowCount > 0)
            return new Registracija(
                results.rows[0].idregistracije,
                results.rows[0].oib,
                results.rows[0].ime,
                results.rows[0].prezime,
                results.rows[0].email,
                new Mjesto(results.rows[0].idmjesta,
                            results.rows[0].pbrmjesta, 
                           results.rows[0].nazivmjesta
                ),
                results.rows[0].administrator,
                results.rows[0].password,
                results.rows[0].slikaosobne,
                results.rows[0].isdentist,
                results.rows[0].finished,
                results.rows[0].accepted
            )
        else return new ErrorInfo(0,"no such registration");
    }
    async getUserRegistrationsUnfinished() {
        var results = await this.client.query(
            `SELECT * FROM KorisnikRegistracija as k join Mjesto as m on k.IdMjesta = m.IdMjesta where finished = false`);
        console.log(results);
        var registracije:Registracija[] = [];
        if(results.rowCount > 0){
            for(var r of results.rows){
                registracije.push(
                    new Registracija(
                        r.idregistracije,
                        r.oib,
                        r.ime,
                        r.prezime,
                        r.email,
                        new Mjesto(r.idmjesta,
                                   r.pbrmjesta, 
                                   r.nazivmjesta
                        ),
                        r.administrator,
                        r.password,
                        r.slikaosobne,
                        r.isdentist,
                        r.finished,
                        r.accepted
                    )
                );
            }
            
        }
        return registracije;
    }

    async getDentists() {
        var results = await this.client.query(`
        SELECT * FROM stomatolog as s JOIN korisnik as k ON s.IdStomatologa = k.IdStomatologa
         JOIN mjesto as m on k.IdMjesta = m.IdMjesta 
         JOIN Specijalizacija as sp ON sp.IdSpecijalizacije = s.IdSpecijalizacije`);
        console.log(results);
        var stomatolozi:Stomatolog[] = [];
        if(results.rowCount > 0){
            for(var r of results.rows){
                stomatolozi.push(
                    new Stomatolog(
                        r.idstomatologa,
                        r.oib,
                        r.ime,
                        r.prezime,
                        r.email,
                        r.password,
                        new Mjesto(r.idmjesta,
                                    r.pbrmjesta, 
                                   r.nazivmjesta
                        ),
                        r.administrator,
                        new Specijalizacija(
                            r.idspecijalizacije,
                            r.nazivspecijalizacije
                        )
                ));
            }
        }
        return stomatolozi;
    }

    async getDentistsBasic() {
        var results = await this.client.query(`
        SELECT * FROM stomatolog as s
         JOIN Specijalizacija as sp ON sp.IdSpecijalizacije = s.IdSpecijalizacije`);
        console.log(results);
        var stomatolozi:Stomatolog[] = [];
        if(results.rowCount > 0){
            for(var r of results.rows){
                stomatolozi.push(
                    new Stomatolog(
                        r.idstomatologa,
                        r.oib,
                        r.ime,
                        r.prezime,
                        r.email,
                        r.password,
                        new Mjesto(r.idmjesta,
                                    r.pbrmjesta, 
                                   r.nazivmjesta
                        ),
                        r.administrator,
                        new Specijalizacija(
                            r.idspecijalizacije,
                            r.nazivspecijalizacije
                        )
                ));
            }
        }
        return stomatolozi;
    }
    async getDentistByIdBasic(id: number) {
        var results = await this.client.query(`
        SELECT * FROM stomatolog as s 
         JOIN Specijalizacija as sp ON sp.IdSpecijalizacije = s.IdSpecijalizacije
         WHERE s.idStomatologa = $1`, [id]);
        console.log(results);
        if(results.rowCount > 0)
            return new Stomatolog(
                results.rows[0].idstomatologa,
                results.rows[0].oib,
                results.rows[0].ime,
                results.rows[0].prezime,
                results.rows[0].email,
                results.rows[0].password,
                new Mjesto(results.rows[0].idmjesta,
                            results.rows[0].pbrmjesta, 
                           results.rows[0].nazivmjesta
                ),
                results.rows[0].administrator,
                new Specijalizacija(
                    results.rows[0].idspecijalizacije,
                    results.rows[0].nazivspecijalizacije, 
                )
            )
        else return new ErrorInfo(0,"no such dentist");
    }
    async getDentistById(id: number) {
        console.log("DENTIST",id);
        
        var results = await this.client.query(`
        SELECT * FROM stomatolog as s JOIN korisnik as k ON s.IdStomatologa = k.IdStomatologa
         JOIN mjesto as m on k.IdMjesta = m.IdMjesta 
         JOIN Specijalizacija as sp ON sp.IdSpecijalizacije = s.IdSpecijalizacije
         WHERE s.idStomatologa = $1`, [id]);
        console.log(results);
        if(results.rowCount > 0)
            return new Stomatolog(
                results.rows[0].idstomatologa,
                results.rows[0].oib,
                results.rows[0].ime,
                results.rows[0].prezime,
                results.rows[0].email,
                results.rows[0].password,
                new Mjesto(results.rows[0].idmjesta,
                            results.rows[0].pbrmjesta, 
                           results.rows[0].nazivmjesta
                ),
                results.rows[0].administrator,
                new Specijalizacija(
                    results.rows[0].idspecijalizacije,
                    results.rows[0].nazivspecijalizacije, 
                )
            )
        else return new ErrorInfo(0,"no such dentist");
    }
    async getDentistByOIB(oib: string) {
        var results = await this.client.query(`
        SELECT * FROM stomatolog as s JOIN korisnik as k ON s.IdStomatologa = k.IdStomatologa
         JOIN mjesto as m on k.IdMjesta = m.IdMjesta 
         JOIN Specijalizacija as sp ON sp.IdSpecijalizacije = s.IdSpecijalizacije
         WHERE k.oib = $1`, [oib]);
        console.log(results);
        if(results.rowCount > 0)
            return new Stomatolog(
                results.rows[0].idstomatologa,
                results.rows[0].oib,
                results.rows[0].ime,
                results.rows[0].prezime,
                results.rows[0].email,
                results.rows[0].password,
                new Mjesto(results.rows[0].idmjesta,
                            results.rows[0].pbrmjesta, 
                           results.rows[0].nazivmjesta
                ),
                results.rows[0].administrator,
                new Specijalizacija(
                    results.rows[0].idspecijalizacije,
                    results.rows[0].nazivspecijalizacije, 
                )
            )
        else return new ErrorInfo(0,"no such dentist");
    }
    async getPlaceByPbr(pbrMjesta: number) {
        var results = await this.client.query(`
        SELECT * FROM mjesto as m WHERE m.pbrMjesta = $1`, [pbrMjesta]);
        console.log(results);
        if(results.rowCount > 0)
            return new Mjesto(  
                results.rows[0].idmjesta,
                results.rows[0].pbrmjesta, 
                results.rows[0].nazivmjesta
            );
        else return new ErrorInfo(0,"no such place");
    }
    async getProcedureById(id: number) {
        var results = await this.client.query(`
        SELECT * FROM Zahvat where SerijskiBrojZahvata = $1`, [id]);
        console.log(results);
        if(results.rowCount > 0){
                return new Zahvat(  
                    results.rows[0].serijskibrojzahvata,
                    results.rows[0].datumzahvata, 
                    results.rows[0].opiszahvata,
                    results.rows[0].idpacijenta,
                );
        }
        else return new ErrorInfo(0,"no such procedures for id");
    }
    async getProceduresFromPacient(pacientId: number) : Promise<Zahvat[]|ErrorInfo> {
        var results = await this.client.query(`
        SELECT * FROM Zahvat where idPacijenta = $1`, [pacientId]);
        console.log(results);
        var zahvati:Zahvat[] = [];
        if(results.rowCount > 0){
            for(var r of results.rows)
            zahvati.push( new Zahvat(  
                r.serijskibrojzahvata,
                new Date(r.datumzahvata), 
                r.opiszahvata,
                r.idpacijenta
            )
            );
            
        }
        return zahvati;
    }
    createProcedure(date: Date, desc: string, pacientId: number) {
        this.client.query(`INSERT INTO Zahvat
        (DatumZahvata, OpisZahvata, IdPacijenta)
        VALUES ($1,$2,$3)`, 
        [date, desc, pacientId]).then((res)=>{}, err=>{
            throw err;
        });
    }
    async getAppointmentById(id: number) {
        var results = await this.client.query(`
        SELECT * FROM Termin where SerijskiBrojTermina = $1`, [id]);
        console.log(results);
        if(results.rowCount > 0){
            return new Termin(  
                results.rows[0].serijskibrojtermina,
                results.rows[0].datumivrijemetermina,
                await this.getPacientByIdBasic(results.rows[0].idpacijenta) as Pacijent,
                await this.getDentistByIdBasic(results.rows[0].idstomatologa) as Stomatolog,
                await this.getTreatmentById(results.rows[0].idtretmana) as Tretman
            );
        }
        else return new ErrorInfo(0,"no such appointments for id");
    }
    async getAppointmentsForPacientById(pacientId: number) {
        var results = await this.client.query(`
        SELECT * FROM Termin where idPacijenta = $1`, [pacientId]);
        console.log(results);
        var termini:Termin[] = [];
        if(results.rowCount > 0){
            for(var r of results.rows){
                termini.push(new Termin(  
                    r.serijskibrojtermina,
                    r.datumivrijemetermina,
                    await this.getPacientById(r.idpacijenta) as Pacijent,
                    await this.getDentistById(r.idstomatologa) as Stomatolog,
                    await this.getTreatmentById(r.idtretmana) as Tretman
                ));
            }
        }
        return termini;
    }
    async getAppointmentsForDentistById(dentistId: number) {
        var results = await this.client.query(`
        SELECT * FROM Termin where idStomatologa = $1`, [dentistId]);
        console.log(results);
        if(results.rowCount > 0){
            var termini:Termin[] = [];
            for(var r of results.rows){
                termini.push(new Termin(  
                    r.serijskibrojtermina,
                    r.datumivrijemetermina,
                    await this.getPacientById(r.idpacijenta) as Pacijent,
                    await this.getDentistById(r.idstomatologa) as Stomatolog,
                    await this.getTreatmentById(r.idtretmana) as Tretman
                ));
            }
            return termini;
        }
        else return new ErrorInfo(0,"no such appointments for dentist");
    }


    async getAppointmentsForTreatmentById(treatmentId: number) {
        var results = await this.client.query(`
        SELECT * FROM Termin where idTretmana = $1`, [treatmentId]);
        console.log(results);
        if(results.rowCount > 0){
            var termini:Termin[] = [];
            for(var r of results.rows){
                termini.push(new Termin(  
                    r.serijskibrojtermina,
                    r.datumivrijemetermina,
                    await this.getPacientById(r.idpacijenta) as Pacijent,
                    await this.getDentistById(r.idstomatologa) as Stomatolog,
                    await this.getTreatmentById(r.idtretmana) as Tretman
                ));
            }
            return termini;
        }
        else return new ErrorInfo(0,"no such treatments for appointment");
    }
    async getAppointmentsByDate(date: Date) {
        var results = await this.client.query(`
        SELECT * FROM Termin where datumIVrijemeTermina = $1`, [date]);
        console.log(results);
        if(results.rowCount > 0){
            var termini:Termin[] = [];
            for(var r of results.rows){
                termini.push(new Termin(  
                    r.serijskibrojtermina,
                    r.datumivrijemetermina,
                    await this.getPacientById(r.idpacijenta) as Pacijent,
                    await this.getDentistById(r.idstomatologa) as Stomatolog,
                    await this.getTreatmentById(r.idtretmana) as Tretman
                ));
            }
            return termini;
        }
        else return new ErrorInfo(0,"no such treatments for pacient");
    }
    async getAppointmentsByMonthAndYear(month: number, year:number) {
        var results = await this.client.query(`
        SELECT * FROM Termin where EXTRACT(MONTH FROM DatumIVrijemeTermina) = $1 AND EXTRACT(YEAR FROM DatumIVrijemeTermina) = $2`, [month, year]);
        console.log(results);
        if(results.rowCount > 0){
            var termini:Termin[] = [];
            for(var r of results.rows){
                termini.push(new Termin(  
                    r.serijskibrojtermina,
                    r.datumivrijemetermina,
                    await this.getPacientById(r.idpacijenta) as Pacijent,
                    await this.getDentistById(r.idstomatologa) as Stomatolog,
                    await this.getTreatmentById(r.idtretmana) as Tretman
                ));
            }
            return termini;
        }
        else return new ErrorInfo(0,"no such treatments for pacient");
    }
    createAppointment(date: Date, pacientId: number, dentistId: number, treatmentId: number) {
        this.client.query(`INSERT INTO Termin
        (DatumIVrijemeTermina, IdPacijenta, IdStomatologa, IdTretmana)
        VALUES ($1,$2,$3,$4)`, 
        [date, pacientId, dentistId, treatmentId]).then((res)=>{}, err=>{
            throw err;
        });
    }
    async getSpecializationById(id: number) {
        var results = await this.client.query(`
        SELECT * FROM Specijalizacija where idSpecijalizacije = $1`, [id]);
        console.log(results);
        if(results.rowCount > 0){
            return new Specijalizacija(
                results.rows[0].idspecijalizacije,
                results.rows[0].nazivspecijalizacije
            )
        }
        else return new ErrorInfo(0,"no such specialization");
    }
    createSpecialization(name: string) {
        this.client.query(`INSERT INTO Specijalizacija
        (NazivSpecijalizacije)
        VALUES ($1)`, 
        [name]).then((res)=>{}, err=>{
            throw err;
        });
    }
    async getUsers() {
        var results = await this.client.query(`SELECT * FROM korisnik as k JOIN mjesto as m on k.IdMjesta = m.IdMjesta`);
        console.log(results);
        if(results.rowCount > 0){
            var korisnici:Korisnik[] = [];
            for(var r of results.rows){
                korisnici.push(
                    new Korisnik(
                        r.oib,
                        r.ime,
                        r.prezime,
                        r.email,
                        r.password,
                        new Mjesto(r.idmjesta,
                                    r.pbrmjesta, 
                                   r.nazivmjesta
                        ),
                        r.administrator,
                        (r.idstomatologa != null)?true:false,
                        r.idstomatologa,
                        r.idpacijenta                    
                    )
                )
            }
            return korisnici;    
        }
        else return new ErrorInfo(0,"no such user");
    }
    async getUserByEmail(email: string) {
        var results = await this.client.query(`SELECT * FROM korisnik as k JOIN mjesto as m on k.IdMjesta = m.IdMjesta WHERE email = $1`, [email]);
        console.log(results);
        if(results.rowCount > 0)
            return new Korisnik(
                results.rows[0].oib,
                results.rows[0].ime,
                results.rows[0].prezime,
                results.rows[0].email,
                results.rows[0].password,
                new Mjesto(results.rows[0].idmjesta,
                            results.rows[0].pbrmjesta, 
                           results.rows[0].nazivmjesta
                ),
                results.rows[0].administrator,
                (results.rows[0].idstomatologa != null)?true:false,
                results.rows[0].idstomatologa,
                results.rows[0].idpacijenta     
            )
        else return new ErrorInfo(0,"no such user");
    }
    async getUserByOIB(oib:string):Promise<ErrorInfo | Korisnik>{//go to db, fetch user
        var results = await this.client.query(`SELECT * FROM korisnik as k JOIN mjesto as m on k.IdMjesta = m.IdMjesta WHERE oib = $1`, [oib]);
        console.log(results);
        if(results.rowCount > 0)
            return new Korisnik(
                results.rows[0].oib,
                results.rows[0].ime,
                results.rows[0].prezime,
                results.rows[0].email,
                results.rows[0].password,
                new Mjesto(results.rows[0].idmjesta,
                            results.rows[0].pbrmjesta, 
                           results.rows[0].nazivmjesta
                ),
                results.rows[0].administrator,
                (results.rows[0].idstomatologa != null)?true:false,
                results.rows[0].idstomatologa,
                results.rows[0].idpacijenta     
            )
        else return new ErrorInfo(0,"no such user");
    }
    async getUsersPaged(pagenum: number, perpage: number) {
        var results = await this.client.query(
            `SELECT * FROM korisnik as k 
            JOIN mjesto as m on k.IdMjesta = m.IdMjesta
            ORDER BY oib
            LIMIT $1
            OFFSET $2`,[perpage, (pagenum-1)*perpage]);
        console.log(results);
        if(results.rowCount > 0){
            var korisnici:Korisnik[] = [];
            for(var r of results.rows){
                korisnici.push(
                    new Korisnik(
                        r.oib,
                        r.ime,
                        r.prezime,
                        r.email,
                        r.password,
                        new Mjesto(r.idmjesta,
                                    r.pbrmjesta, 
                                   r.nazivmjesta
                        ),
                        r.administrator,
                        (r.idstomatologa != null)?true:false,
                        r.idstomatologa,
                        r.idpacijenta     
                    )
                )
            }
            return korisnici;    
        }
        else return new ErrorInfo(0,"no such user");
    }
    async getTreatmentById(id: number) : Promise<Tretman|ErrorInfo> {
        var results = await this.client.query(`
        SELECT * FROM Tretman as t WHERE t.idTretmana = $1`, [id]);
        console.log(results);
        if(results.rowCount > 0)
            return new Tretman(  
                results.rows[0].idtretmana,
                results.rows[0].nazivtretmana, 
                results.rows[0].cijenatretmana,
            );
        else return new ErrorInfo(0,"no such treatment");
    }
    async getTreatments() {
        var results = await this.client.query(`
        SELECT * FROM Tretman`);
        console.log(results);
        if(results.rowCount > 0){
            var tretmani:Tretman[] = [];
            for(var r of results.rows)
            tretmani.push(  new Tretman(  
                r.idtretmana,
                r.nazivtretmana, 
                r.cijenatretmana,
            )
            );
            return tretmani;
        }
        else return new ErrorInfo(0,"no such treatments");
    }
    async getTreatmentsPaged(pagenum: number, perpage: number) {
        var results = await this.client.query(`
        SELECT * FROM Tretman order by idTretmana LIMIT $1 OFFSET $2`, [perpage, (pagenum-1)*perpage]);
        console.log(results);
        if(results.rowCount > 0){
            var tretmani:Tretman[] = [];
            for(var r of results.rows)
            tretmani.push(  new Tretman(  
                r.idtretmana,
                r.nazivtretmana, 
                r.cijenatretmana,
            )
            );
            return tretmani;
        }
        else return new ErrorInfo(0,"no such treatments");
    }
    // async getTreatmentsByPacientId(pacientId: number) : Promise<Tretman[]|ErrorInfo> {
    //     var results = await this.client.query(`
    //     SELECT * FROM Tretman as t WHERE t.idPacijenta = $1`, [pacientId]);
    //     console.log(results);
    //     if(results.rowCount > 0){
    //         var tretmani:Tretman[] = [];
    //         for(var r of results.rows){
    //             tretmani.push(new Tretman(  
    //                 r.idtretmana,
    //                 r.nazivtretmana, 
    //                 r.cijenatretmana,
    //             ));
    //         }
    //         return tretmani;
    //     }
    //     else return new ErrorInfo(0,"no such treatment");
    // }
    createTreatment(name: string, price: number) {
        this.client.query(`INSERT INTO Tretman
        (NazivTretmana, CijenaTretmana)
        VALUES ($1,$2)`, 
        [name, price]).then((res)=>{}, err=>{
            throw err;
        });
    }
    async getPacients() {
        var results = await this.client.query(`
        SELECT * FROM pacijent as p JOIN korisnik as k ON p.idpacijenta = k.idpacijenta
         JOIN mjesto as m on k.IdMjesta = m.IdMjesta`);
        console.log(results);
        if(results.rowCount > 0){
            var pacijenti:Pacijent[] = [];
            for(var r of results.rows){
                pacijenti.push(
                    new Pacijent(
                        r.idpacijenta,
                        r.oib,
                        r.ime,
                        r.prezime,
                        r.email,
                        r.password,
                        new Mjesto(r.idmjesta,
                                    r.pbrmjesta, 
                                   r.nazivmjesta
                        ),
                        r.administrator,
                        await this.getProceduresFromPacient(r.idpacijent) as Zahvat[],
                        r.statuszuba
                    )
                )
            }
            return pacijenti;
        }
        else return new ErrorInfo(0,"no such Pacient");
    }

    async getPacientsBasic() {
        var results = await this.client.query(`
        SELECT * FROM pacijent`);
        console.log(results);
        if(results.rowCount > 0){
            var pacijenti:Pacijent[] = [];
            for(var r of results.rows){
                pacijenti.push(
                    new Pacijent(
                        r.idpacijenta,
                        r.oib,
                        r.ime,
                        r.prezime,
                        r.email,
                        r.password,
                        new Mjesto(r.idmjesta,
                                    r.pbrmjesta, 
                                   r.nazivmjesta
                        ),
                        r.administrator,
                        await this.getProceduresFromPacient(r.idpacijenta) as Zahvat[],
                        r.statuszuba
                    )
                )
            }
            return pacijenti;
        }
        else return new ErrorInfo(0,"no such Pacient");
    }
    async getPacientById(id: number) : Promise<Pacijent|ErrorInfo> {
        var results = await this.client.query(`
        SELECT * FROM pacijent as p JOIN korisnik as k ON p.idpacijenta = k.idpacijenta
         JOIN mjesto as m on k.IdMjesta = m.IdMjesta 
         WHERE p.idpacijenta = $1`, [id]);
        console.log(results);
        if(results.rowCount > 0)
            return new Pacijent(
                results.rows[0].idpacijenta,
                results.rows[0].oib,
                results.rows[0].ime,
                results.rows[0].prezime,
                results.rows[0].email,
                results.rows[0].password,
                new Mjesto(results.rows[0].idmjesta,
                            results.rows[0].pbrmjesta, 
                           results.rows[0].nazivmjesta
                ),
                results.rows[0].administrator,
                await this.getProceduresFromPacient(results.rows[0].idpacijenta) as Zahvat[],
                results.rows[0].statuszuba
            )
        else return new ErrorInfo(0,"no such Pacient");
    }

    async getPacientByOIB(oib: string) {
        var results = await this.client.query(`
        SELECT * FROM pacijent as p JOIN korisnik as k ON p.idpacijenta = k.idpacijenta
         JOIN mjesto as m on k.IdMjesta = m.IdMjesta 
         WHERE k.oib = $1`, [oib]);
        console.log(results);
        if(results.rowCount > 0)
            return new Pacijent(
                results.rows[0].idpacijenta,
                results.rows[0].oib,
                results.rows[0].ime,
                results.rows[0].prezime,
                results.rows[0].email,
                results.rows[0].password,
                new Mjesto(results.rows[0].idmjesta,
                            results.rows[0].pbrmjesta, 
                           results.rows[0].nazivmjesta
                ),
                results.rows[0].administrator,
                await this.getProceduresFromPacient(results.rows[0].idpacijenta) as Zahvat[],
                results.rows[0].statuszuba
            )
        else return new ErrorInfo(0,"no such Pacient");
    }
    createPacient(oib: string, ime: string, prezime: string, email: string, password:string, idMjesta: number) {
        this.client.query(`INSERT INTO Pacijent
        (StatusZuba)
        VALUES ('Sve uredu') RETURNING IdPacijenta`).then((res)=>{
            this.client.query(`INSERT INTO Korisnik
                (OIB, Ime, Prezime, Email, Password, IdMjesta, IdPacijenta)
                VALUES ($1,$2,$3,$4,$5,$6,$7)`, 
                [oib, ime, prezime, email, password, idMjesta, res.rows[0].idpacijenta]
                ).then((res)=>{}, err=>{
                    throw err;
                });
        }, err=>{   
            throw err;
        });
    }
    createDentist(oib: string, ime: string, prezime: string, email: string, password:string, idMjesta: number, idSpecijalizacije:number){
        this.client.query(`INSERT INTO Stomatolog
        (IdSpecijalizacije)
        VALUES ($1) RETURNING IdPacijenta`, [idSpecijalizacije]).then((res)=>{
            this.client.query(`INSERT INTO Korisnik
                (OIB, Ime, Prezime, Email, Password, IdMjesta, IdStomatologa)
                VALUES ($1,$2,$3,$4,$5,$6,$7)`, 
                [oib, ime, prezime, email, password, idMjesta, res.rows[0].idStomatologa]
                ).then((res)=>{}, err=>{
                    throw err;
                });
        }, err=>{   
            throw err;
        });
    }
    async getPlaces() {
        var results = await this.client.query(`
        SELECT * FROM mjesto`);
        console.log(results);
        if(results.rowCount > 0){
            var mjesta:Mjesto[] = [];
            for(var r of results.rows)
            mjesta.push( new Mjesto(  
                r.idmjesta,
                r.pbrmjesta, 
                r.nazivmjesta
            )
            );
            return mjesta;
        }
        else return new ErrorInfo(0,"no such place");
    }
    async getPlaceById(id:number) {
        var results = await this.client.query(`
        SELECT * FROM mjesto as m WHERE m.idMjesta = $1`, [id]);
        console.log(results);
        if(results.rowCount > 0)
            return new Mjesto(  
                results.rows[0].idmjesta,
                results.rows[0].pbrmjesta, 
                results.rows[0].nazivmjesta
            );
        else return new ErrorInfo(0,"no such place");
    }
    createPlace(pbrMjesta: number, naziv: string) {
        this.client.query(`INSERT INTO Mjesto
        (PbrMjesta, NazivMjesta)
        VALUES ($1,$2)`, 
        [pbrMjesta, naziv]).then((res)=>{}, err=>{
            throw err;
        });
    }
    async init(){
        await this.client.connect();
        //await this.createDB();
        //********************
        // za init baze
        //********************
        // this.createDB().then(()=>{
        //     this.populate();
        // });
    }
    
    getUser(username:string){
        return {username: "user", password:"password"};
    }

    async createDB(){
        await this.client.query(`
            CREATE TABLE IF NOT EXISTS Pacijent
            (
                IdPacijenta SERIAl,
                statusZuba TEXT,
                PRIMARY KEY (IdPacijenta)
            );
        
            
            CREATE TABLE IF NOT EXISTS Specijalizacija
            (
                IdSpecijalizacije SERIAl,
                NazivSpecijalizacije VARCHAR(25) NOT NULL,
                PRIMARY KEY (IdSpecijalizacije)
            );
            
            CREATE TABLE IF NOT EXISTS Tretman
            (
                IdTretmana SERIAl,
                NazivTretmana VARCHAR(40) NOT NULL,
                CijenaTretmana INT NOT NULL,
                PRIMARY KEY (IdTretmana)
            );
            
            CREATE TABLE IF NOT EXISTS Mjesto
            (
                IdMjesta SERIAL,
                PbrMjesta INT NOT NULL,
                NazivMjesta VARCHAR(20) NOT NULL,
                PRIMARY KEY (IdMjesta)
            );
            
            CREATE TABLE IF NOT EXISTS Zahvat
            (
                SerijskiBrojZahvata SERIAL,
                DatumZahvata DATE NOT NULL,
                OpisZahvata VARCHAR(150) NOT NULL,
                IdPacijenta INT NOT NULL,
                PRIMARY KEY (SerijskiBrojZahvata),
                FOREIGN KEY (IdPacijenta) REFERENCES Pacijent(IdPacijenta)
            );
            CREATE TABLE IF NOT EXISTS Stomatolog
            (
                IdStomatologa SERIAL,
                IdSpecijalizacije INT NOT NULL,
                PRIMARY KEY (IdStomatologa),
                FOREIGN KEY (IdSpecijalizacije) REFERENCES Specijalizacija(IdSpecijalizacije)
            );
            CREATE TABLE IF NOT EXISTS Korisnik
            (
                OIB VARCHAR(11) NOT NULL,
                Ime VARCHAR(30) NOT NULL,
                Prezime VARCHAR(30) NOT NULL,
                Email VARCHAR(128) NOT NULL UNIQUE,
                Password VARCHAR(128) NOT NULL,
                IdMjesta INT NOT NULL,
                IdPacijenta INT,
                IdStomatologa INT,
                Administrator BOOLEAN DEFAULT FALSE,
                PRIMARY KEY (OIB),
                FOREIGN KEY (IdMjesta) REFERENCES Mjesto(IdMjesta),
                FOREIGN KEY (IdPacijenta) REFERENCES Pacijent(IdPacijenta),
                FOREIGN KEY (IdStomatologa) REFERENCES Stomatolog(IdStomatologa)
            );
            CREATE TABLE IF NOT EXISTS Termin
            (
                SerijskiBrojTermina SERIAL,
                DatumIVrijemeTermina DATE NOT NULL,
                IdPacijenta INT NOT NULL,
                IdStomatologa INT NOT NULL,
                IdTretmana INT NOT NULL,
                PRIMARY KEY (SerijskiBrojTermina),
                FOREIGN KEY (IdPacijenta) REFERENCES Pacijent(IdPacijenta),
                FOREIGN KEY (IdStomatologa) REFERENCES Stomatolog(IdStomatologa),
                FOREIGN KEY (IdTretmana) REFERENCES Tretman(IdTretmana)
            );
            CREATE TABLE IF NOT EXISTS KorisnikRegistracija
            (
                idRegistracije SERIAL,
                OIB VARCHAR(11) NOT NULL,
                Ime VARCHAR(30) NOT NULL,
                Prezime VARCHAR(30) NOT NULL,
                Email VARCHAR(128) NOT NULL UNIQUE,
                Password VARCHAR(128) NOT NULL,
                SlikaOsobne BYTEA NOT NULL,
                IdMjesta INT NOT NULL,
                isDentist BOOLEAN DEFAULT FALSE,
                finished BOOLEAN DEFAULT FALSE,
                accepted BOOLEAN DEFAULT FALSE,
                Administrator BOOLEAN DEFAULT FALSE,
                PRIMARY KEY (idRegistracije),
                FOREIGN KEY (IdMjesta) REFERENCES Mjesto(IdMjesta)
            );
        `);
    }

    async populate(){
        await this.client.query(`
            INSERT INTO mjesto(pbrmjesta, nazivmjesta)
                VALUES
                    (10000, 'Zagreb'),
                    (21000, 'Split'),
                    (51000, 'Rijeka'),
                    (31000, 'Osijek'),
                    (23000, 'Zadar'),
                    (35000, 'Slavonski Brod'),
                    (52100, 'Pula'),
                    (47000, 'Karlovac'),
                    (44000, 'Sisak'),
                    (20000, 'Dubrovnik'),
                    (42000, 'Varaždin'),
                    (22000, 'Šibenik');
            
            INSERT INTO specijalizacija(nazivspecijalizacije)
                VALUES
                    ('RTG dijagnostika'),
                    ('Oralna kirurgija'),
                    ('Dječja stomatologija'),
                    ('Ortodoncija'),
                    ('Endodoncija'),
                    ('Stomatološka protetika'),
                    ('Parodontologija');
            
            INSERT INTO tretman(nazivtretmana, cijenatretmana)
                VALUES
                    ('Redoviti pregled', 0),
                    ('Operacija zuba - vađenje', 50),
                    ('Ugradnja mosta', 250),
                    ('Izrada proteze', 200),
                    ('Brušenje proteze', 40);
            `)
    }
}

export {PostgresDAO}