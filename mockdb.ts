import { DAO } from "./dao";
import { Mjesto, Pacijent } from "./models";

class MockDB implements DAO{
    pacijenti:Pacijent[] = []
    getAppointments() {
        throw new Error("Method not implemented.");
    }
    getDentistByIdBasic(id: any) {
        throw new Error("Method not implemented.");
    }
    createDentistBasic(idSpecijalizacije: any) {
        throw new Error("Method not implemented.");
    }
    getProcedures() {
        throw new Error("Method not implemented.");
    }
    getUserByOIB(oib: string) {
        throw new Error("Method not implemented.");
    }
    getUserByEmail(email: string) {
        throw new Error("Method not implemented.");
    }
    getUsers() {
        throw new Error("Method not implemented.");
    }
    getUsersPaged(pagenum: number, perpage: number) {
        throw new Error("Method not implemented.");
    }
    deleteUserByOIB(oib: string) {
        throw new Error("Method not implemented.");
    }
    updateUserByOIB(oib: string, ime: string, prezime: string, email: string, password: string, idMjesta: number, idStomatologa: number, idPacijenta: number, administrator: boolean) {
        throw new Error("Method not implemented.");
    }
    createUserRegistration(oib: string, ime: string, prezime: string, email: string, password: string, idMjesta: number, slikaOsobne: Blob, isDentist: boolean) {
        throw new Error("Method not implemented.");
    }
    getUserRegistrations() {
        throw new Error("Method not implemented.");
    }
    getUserRegistrationById(id: number) {
        throw new Error("Method not implemented.");
    }
    getUserRegistrationByOIB(oib: string) {
        throw new Error("Method not implemented.");
    }
    getUserRegistrationsUnfinished() {
        throw new Error("Method not implemented.");
    }
    finishRegistration(id: number, accept: boolean) {
        throw new Error("Method not implemented.");
    }
    getPacientById(id: number) {
        return this.pacijenti[id];
    }
    getPacientByIdBasic(id: number) {
        return this.pacijenti[id];
    }
    getPacients() {
        throw new Error("Method not implemented.");
    }
    getPacientsBasic() {
        return this.pacijenti;
    }
    getPacientByOIB(oib: string) {
        throw new Error("Method not implemented.");
    }
    createPacient(oib: string, ime: string, prezime: string, email: string, password: string, idMjesta: number) {
        throw new Error("Method not implemented.");
    }
    createBasicPacient(statusZuba: string) {
      this.pacijenti.push( new Pacijent(this.pacijenti.length, "","","","","",new Mjesto(1,1,""),true,[],statusZuba));
    }
    deletePacientByOIB(oib: string) {
        throw new Error("Method not implemented.");
    }
    deletePacientById(id: number) {
        this.pacijenti.splice(id, 1);
    }
    updatePacientById(id: number, statusZuba: string) {
        this.pacijenti[id].statusZuba = statusZuba;
    }
    getDentistById(id: number) {
        throw new Error("Method not implemented.");
    }
    getDentists() {
        throw new Error("Method not implemented.");
    }
    getDentistsBasic() {
        throw new Error("Method not implemented.");
    }
    getDentistByOIB(oib: string) {
        throw new Error("Method not implemented.");
    }
    createDentist(oib: string, ime: string, prezime: string, email: string, password: string, idMjesta: number, idSpecijalizacije: number) {
        throw new Error("Method not implemented.");
    }
    deleteDentistByOIB(oib: string) {
        throw new Error("Method not implemented.");
    }
    deleteDentistById(id: number) {
        throw new Error("Method not implemented.");
    }
    updateDentistById(id: number, idSpecijalizacije: number) {
        throw new Error("Method not implemented.");
    }
    getPlaces() {
        throw new Error("Method not implemented.");
    }
    getPlaceById(id: number) {
        throw new Error("Method not implemented.");
    }
    getPlaceByPbr(pbrMjesta: number) {
        throw new Error("Method not implemented.");
    }
    createPlace(pbrMjesta: number, naziv: string) {
        throw new Error("Method not implemented.");
    }
    deletePlaceById(id: number) {
        throw new Error("Method not implemented.");
    }
    deletePlaceByPbr(pbrMjesta: number) {
        throw new Error("Method not implemented.");
    }
    updatePlaceById(id: number, pbrMjesta: number, naziv: string) {
        throw new Error("Method not implemented.");
    }
    updatePlaceByPbr(pbrMjesta: number, naziv: string) {
        throw new Error("Method not implemented.");
    }
    getProcedureById(id: number) {
        throw new Error("Method not implemented.");
    }
    getProceduresFromPacient(pacientId: number) {
        throw new Error("Method not implemented.");
    }
    createProcedure(date: Date, desc: string, pacientId: number) {
        throw new Error("Method not implemented.");
    }
    deleteProcedureById(id: number) {
        throw new Error("Method not implemented.");
    }
    deleteProcedureByPacientId(pacientId: number) {
        throw new Error("Method not implemented.");
    }
    updateProcedureById(id: number, date: Date, desc: string, pacientId: number) {
        throw new Error("Method not implemented.");
    }
    getAppointmentById(id: number) {
        throw new Error("Method not implemented.");
    }
    getAppointmentsForPacientById(pacientId: number) {
        throw new Error("Method not implemented.");
    }
    getAppointmentsForDentistById(dentistId: number) {
        throw new Error("Method not implemented.");
    }
    getAppointmentsForTreatmentById(treatmentId: number) {
        throw new Error("Method not implemented.");
    }
    getAppointmentsByDate(date: Date) {
        throw new Error("Method not implemented.");
    }
    createAppointment(date: Date, pacientId: number, dentistId: number, treatmentId: number) {
        throw new Error("Method not implemented.");
    }
    deleteAppointmentById(id: number) {
        throw new Error("Method not implemented.");
    }
    deleteAppointmentsForPacientById(pacientId: number) {
        throw new Error("Method not implemented.");
    }
    deleteAppointmentsForDentistById(dentistId: number) {
        throw new Error("Method not implemented.");
    }
    deleteAppointmentsForTreatmentById(treatmentId: number) {
        throw new Error("Method not implemented.");
    }
    deleteAppointmentsByDate(date: Date) {
        throw new Error("Method not implemented.");
    }
    updateAppointment(id: number, date: Date, pacientId: number, dentistId: number, treatmentId: number) {
        throw new Error("Method not implemented.");
    }
    getSpecializationById(id: number) {
        throw new Error("Method not implemented.");
    }
    getSpecializations() {
        throw new Error("Method not implemented.");
    }
    createSpecialization(name: string) {
        throw new Error("Method not implemented.");
    }
    deleteSpecializationById(id: number) {
        throw new Error("Method not implemented.");
    }
    updateSpecialization(id: number, name: string) {
        throw new Error("Method not implemented.");
    }
    getTreatmentById(id: number) {
        throw new Error("Method not implemented.");
    }
    getTreatments() {
        throw new Error("Method not implemented.");
    }
    getTreatmentsPaged(pagenum: number, perpage: number) {
        throw new Error("Method not implemented.");
    }
    createTreatment(name: string, price: number) {
        throw new Error("Method not implemented.");
    }
    deleteTreatmentById(id: number) {
        throw new Error("Method not implemented.");
    }
    updateTreatment(id: number, name: string, price: number) {
        throw new Error("Method not implemented.");
    }
    init(): void {
        throw new Error("Method not implemented.");
    }
    createDB(): void {
        throw new Error("Method not implemented.");
    }
    populate(): void {
        throw new Error("Method not implemented.");
    }
    
}

export {MockDB}